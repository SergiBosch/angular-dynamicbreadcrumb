export const translations = {
    title: 'Breadcrumb',
    breadcrumb: {
        labelHome: 'Inicio ',
        labelStatic: 'Estatico ',
        labelDynamic: 'Dinamico '
    },
    menu: {
        test1: 'Prueba breadcrumb 1',
        test2: 'Prueba breadcrumb 2',
        test3: 'Prueba breadcrumb 3',
        description1: 'Breadcrumb todo estático saltando una parte',
        description2: 'Breadcrumb todo dinamico saltando una parte',
        description3: 'Breadcrumb con parte dinamica y con parte estatica',
    }
}
