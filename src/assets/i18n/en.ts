export const translations = {
    title: 'Breadcrumb',
    breadcrumb: {
        labelHome: 'Home ',
        labelStatic: 'Static ',
        labelDynamic: 'Dynamic '
    },
    menu: {
        test1: 'Test breadcrumb 1',
        test2: 'Test breadcrumb 2',
        test3: 'Test breadcrumb 3',
        description1: 'All static breadcrumb with an omitted part',
        description2: 'All dynamic breadcrumb with an omitted part',
        description3: 'Static and dynamic breadcrumb',
    }
}
