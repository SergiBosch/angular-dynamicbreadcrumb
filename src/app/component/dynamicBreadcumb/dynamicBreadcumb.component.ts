import { BreadcrumbOptions } from './../../interfaces/breadcrumbOptions.interface';
import { IBreadCrumb } from '../../interfaces/breadcrumb.interface';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, Event } from '@angular/router';

import { filter, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './dynamicBreadcumb.component.html',
  styleUrls: ['./dynamicBreadcumb.component.scss']
})

export class DynamicBreadcumbComponent implements OnInit {
  public breadcrumbs: IBreadCrumb[]

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
  }

  ngOnInit() {
    this.router.events.pipe(
      filter((event: Event) => event instanceof NavigationEnd),
      distinctUntilChanged(),
    ).subscribe(() => {
      this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
    })
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadCrumb[] = []): IBreadCrumb[] {
    if (route.firstChild) {
      //If we are on the root path we need to execute again
      return this.buildBreadCrumb(route.firstChild);
    }
    if (route.routeConfig && route.routeConfig.data) {
      //If no routeConfig is avalailable we are on the root path
      let breadcrumbOptions: BreadcrumbOptions[] = route.routeConfig.data.breadcrumb;
      let path = route.routeConfig.path;

      //Array of
      let newBreadcrumbs: IBreadCrumb[] = []

      //Split URL
      let pathSplited = path.split('/');
      //Inicialize URLs for each part of breadcrumb
      let newUrl = ''

      for (let i = 0; i < pathSplited.length; i++) {
        //Select Label and create URLs for labels
        newUrl = newUrl + '/' + pathSplited[i];
        let breadcrumbObject = breadcrumbOptions[i];
        let writenLabel = breadcrumbObject.label;
        let dynamicLabel = '';

        // If the route is dynamic route such as ':id', remove it
        let isDynamicRoute = pathSplited[i].startsWith(':');
        if (isDynamicRoute && !!route.snapshot) {
          let paramName = pathSplited[i].split(':').pop();
          newUrl = newUrl.replace(pathSplited[i], route.snapshot.params[paramName]);
          if(breadcrumbObject.dynamic){
            dynamicLabel = route.snapshot.params[paramName];
          }
        }

        //Check if we want to write our label
        if (breadcrumbObject.label || breadcrumbObject.dynamic) {
          //Build breadcrumb object
          let breadcrumb: IBreadCrumb = {
            label: writenLabel,
            url: newUrl,
            dynamicLabel: dynamicLabel
          }

          //Push breadcrumb object to array
          newBreadcrumbs.push(breadcrumb);
        }

      }

      //return new array of breadcrumbs
      return newBreadcrumbs;
    }
  }
}