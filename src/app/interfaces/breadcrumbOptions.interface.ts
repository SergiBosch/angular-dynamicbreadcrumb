export interface BreadcrumbOptions {
    label: string;
    dynamic?: boolean;
}
