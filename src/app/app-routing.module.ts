import { ComponentComponent } from './pages/component/component.component';
import { MenuComponent } from './pages/menu/menu.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  { path: 'home', component: MenuComponent, data: { breadcrumb: [{ label:'breadcrumb.labelHome' }] } },
  { path: 'home/static1/static2', component: ComponentComponent, data: {breadcrumb: [{ label: 'breadcrumb.labelHome' },{ label: '' },{ label: 'breadcrumb.labelStatic' }]}},
  { path: 'home/:id/:id2/:id3', component: ComponentComponent, data: {breadcrumb: [{ label: 'breadcrumb.labelHome' },{ label: 'breadcrumb.labelDynamic', dynamic: true },{ label: 'breadcrumb.labelDynamic'},{ label: '', dynamic: true }]}},
  { path: 'home/:id/static1', component: ComponentComponent, data: {breadcrumb: [{ label: 'breadcrumb.labelHome' },{ label: '', dynamic: true },{label: 'breadcrumb.labelStatic'}]}},
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
