import { environment } from './../environments/environment';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { translations as es } from '../assets/i18n/es';
import { translations as en } from '../assets/i18n/en';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-prova';
  locales = ['es', 'en']

  constructor(public translate: TranslateService){
    translate.setDefaultLang(environment.lang);
    translate.use(environment.lang)
    translate.setTranslation('es', es);
    translate.setTranslation('en', en)
  }
}
